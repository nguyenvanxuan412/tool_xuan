import click
from os import system


@click.command()
@click.argument('sub_env')
def new_deploy(sub_env):
    """
    tool_xuan neospine <sub-env>
    """
    command = (f'dev tf devad/patient-referral -- apply -target=aws_ssm_parameter.api_app_host[\\"{sub_env}\\"] '
               f'-target=aws_ssm_parameter.project_code[\\"{sub_env}\\"] '
               f'-target=module.infra.aws_ssm_parameter.capacity_provider[\\"{sub_env}\\"] '
               f'-target=module.infra.aws_ssm_parameter.disable_components[\\"{sub_env}\\"] '
               f'-target=module.infra.module.codebuild.aws_codebuild_project.all[\\"{sub_env}\\"] '
               f'-target=module.infra.module.codebuild.aws_codebuild_webhook.all[\\"{sub_env}\\"]')
    print(command)
    system(command)

# new_deploy("pr-neospine-dev1")
