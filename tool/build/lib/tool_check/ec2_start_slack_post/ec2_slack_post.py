import requests

SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"


def response(slack_webhook_url, data: dict):
    headers = {'Content-type': 'application/json'}
    requests.post(SLACK_WEBHOOK_URL, headers=headers, data=data)