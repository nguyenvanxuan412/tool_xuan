import os
import requests
import json
from datetime import datetime, timedelta
import pytz

SLACK_CHANNEL = "#random"
now = datetime.now(pytz.utc)
date_commit = now + timedelta(hours=7)
date_prime = date_commit.strftime("%d/%m/%Y - %H:%M:%S")


def request(service: str, aws_account: str, ec2_list):
    blocks = ec2_list
    blocks += {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": f'---------------------------------------'
        }
    },
    message = {
        "channel": SLACK_CHANNEL,
        "icon_emoji": ":pipeline11:",
        "attachments": [
            {
                "color": "#2eb886",
                "blocks": [
                              {
                                  "type": "section",
                                  "text": {
                                      "type": "mrkdwn",
                                      "text": f':clap: *EC2 Status {aws_account}*'
                                  }
                              },
                              {
                                  "type": "section",
                                  "fields": [
                                      {
                                          "type": "mrkdwn",
                                          "text": f'*Type:*\n *{service}*'
                                      },
                                      {
                                          "type": "mrkdwn",
                                          "text": f'*When:*\n{date_prime} '
                                      }
                                  ]
                              },
                              {
                                  "type": "section",
                                  "text": {
                                      "type": "mrkdwn",
                                      "text": "<https://us-west-2.console.aws.amazon.com/ec2/home?region=us-west-2"
                                              "#Instances:v=3;$case=tags:true%5C,client:false;$regex=tags:false%5C,"
                                              "client:false|View AWS Console>"
                                  }
                              }
                          ]
                          + blocks
            }

        ]
    }
    json_message = json.dumps(message)

    # Set the headers and data for the HTTP POST request
    data = json_message

    return data
