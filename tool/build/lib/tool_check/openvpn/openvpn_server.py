from os import system
import os
import subprocess
import re
import shlex

openvpn_config = "/etc/openvpn/server/client-common.txt"

ip_address_pattern = re.compile(r'\b(?:\d{1,3}\.){3}\d{1,3}\b')


def run_cmd(command: str, cwd=None, show=False, check=False):
    command_items = shlex.split(command)
    my_env = os.environ.copy()
    out_put = subprocess.run([command], shell=True, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                             text=True, env=my_env)
    combined_output = out_put.stdout
    if show:
        print(combined_output)

    return combined_output


command = "curl http://169.254.169.254/latest/meta-data/public-ipv4"
public_ip = run_cmd(command=command)
ip_address = ip_address_pattern.search(public_ip)
new_address = ip_address.group()
print(new_address)
new_line = f"remote {new_address} 1194\n"
file_read = openvpn_config
with open(file_read, 'r', encoding='utf-8') as file:
    data = file.readlines()
data[3] = new_line
with open(file_read, 'w', encoding='utf-8') as file:
    file.writelines(data)

openvpn_restart = "service openvpn-server@server restart"
system(openvpn_restart)
