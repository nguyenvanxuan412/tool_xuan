from tool_check.utils.aws_account import account_id
from tool_check.xuan_abc.aws_password import aws
from tool_check.utils.capture_feature import capture
from tool_check.utils.devad_bastion_start import start_bastion_addy
from tool_check.awscost.aws_cost import cost
from tool_check.utils.email_xuan import email
from tool_check.awscost.aws_cost import costmonthly
from tool_check.utils.ec2_utils import ec2_public_ip, ec2_connet, ec2_send_cli
from tool_check.utils.ssh_nat import ssh
from tool_check.utils.nhasach import nhasach
from tool_check.utils.nhasach import nhasach_gn
from tool_check.utils.get_core_param_print import pr
from tool_check.utils.aws_prescriptive import aws_prescriptive
from tool_check.utils.lambda_utils import call_lambda
from tool_check.utils.docs import docs
from tool_check.utils.dev_ci_xuan_s3_upload import s3_ci
from tool_check.utils.dev_ci_xuan_s3_upload import s3_dev
from tool_check.utils.codebuild_xuan import codebuild_xuan
from tool_check.utils.aws_cicd_whitepaper import aws_cicd
from tool_check.utils.sshuttle import sshuttle
from tool_check.utils.project_infra import project_infra
from tool_check.utils.exam_aws_pro import exam
from tool_check.awscost.aws_cost import costspdate
from tool_check.utils.aws_network import network
from tool_check.utils.aws_devops_guidance import devops
from tool_check.utils.aws_iam import iam, group_for_user
from tool_check.neospine_deploy.neospine_deploy import new_deploy
from tool_check.tp7_credentials.tp7_credentials import tp7
from tool_check.utils.google_meeting_anh_an import meeting
from tool_check.utils.link_learn_aws import link_aws
from tool_check.openvpn.xuan_vpn import vpn_connect, vpn_disconnect, run_vpn_server
from tool_check.aws_lab.rds_lab import rds_lab
from tool_check.aws_profile_secret_value_pr.set_secret_value import secret_client
import click
from tool_check.utils.eng_translate import eng_transslate
from tool_check.utils.daily_task import daily_task
from tool_check.utils.youtube_download import download_youtube

@click.group()
def cli():
    """
    Dev Tool - A tool developed by Xuan to support build, deploy projects developed by TenPoint7 and much more
    \f

    :param debug:
    :param kwargs:
    :return:
    """


cli.add_command(aws)
cli.add_command(account_id)
cli.add_command(capture)
cli.add_command(start_bastion_addy)
cli.add_command(cost)
cli.add_command(costspdate)
cli.add_command(costmonthly)
cli.add_command(ec2_send_cli, name='sendcli')
cli.add_command(ec2_connet, name='connect')
cli.add_command(ec2_public_ip, name='public')
cli.add_command(ssh)
cli.add_command(email)
cli.add_command(nhasach)
cli.add_command(nhasach_gn, name='nhasachgn')
cli.add_command(pr)
cli.add_command(aws_prescriptive, name="awspr")
cli.add_command(call_lambda, name="lambda")
cli.add_command(docs, name="doc")
cli.add_command(s3_ci)
cli.add_command(s3_dev)
cli.add_command(codebuild_xuan, name="cbxuan")
cli.add_command(aws_cicd, name="cicd")
cli.add_command(sshuttle)
cli.add_command(project_infra, name="project-infra")
cli.add_command(exam)
cli.add_command(network)
cli.add_command(devops)
cli.add_command(iam)
cli.add_command(group_for_user, name="group")
cli.add_command(new_deploy, name="neospine")
cli.add_command(tp7, name="tp7")
cli.add_command(meeting)
cli.add_command(link_aws, name="link")
cli.add_command(vpn_connect, name="vpn")
cli.add_command(vpn_disconnect, name="vpndis")
cli.add_command(run_vpn_server,name="vpnrun")
cli.add_command(rds_lab,name="rdslab")
cli.add_command(secret_client, name="pr-secret")
cli.add_command(eng_transslate, name="dich")
cli.add_command(daily_task, name="task")
cli.add_command(download_youtube, name="youtube")

if __name__ == '__main__':
    cli()
