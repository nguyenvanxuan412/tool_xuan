import click
import pyperclip


@click.command()
@click.argument("number")
def network(number):
    """
    tool_xuan network <number> . number should 1 or 2 or 3
    """
    if number == "1":
        pyperclip.copy("https://docs.aws.amazon.com/whitepapers/latest/building-scalable-secure-multi-vpc-network-infrastructure/welcome.html")
        click.echo(click.style('Building a Scalable and Secure Multi-VPC AWS Network Infrastructure !', fg='green'))
    elif number == "2":
        pyperclip.copy(
            "https://docs.aws.amazon.com/whitepapers/latest/aws-vpc-connectivity-options/welcome.html")
        click.echo(click.style('Amazon Virtual Private Cloud Connectivity Options !', fg='green'))
    elif number == "3":
        pyperclip.copy(
            "https://aws.amazon.com/blogs/networking-and-content-delivery/how-to-enhance-cloudfront-origin-security-of-on-premise-web-servers-using-third-party-firewalls/#:~:text=CloudFront%20acts%20as%20a%20security,attacks%20from%20reaching%20your%20origin.")
        click.echo(click.style('How to enhance CloudFront origin security of on-premise web servers using third-party firewalls !', fg='green'))
    elif number == "4":
        pyperclip.copy(
            "https://docs.aws.amazon.com/whitepapers/latest/aws-best-practices-ddos-resiliency/aws-best-practices-ddos-resiliency.html")
        click.echo(click.style(
            'AWS Best Practices for DDoS Resiliency !',
            fg='green'))





