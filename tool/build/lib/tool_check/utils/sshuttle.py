import click
from os import system


@click.command()
@click.argument("profile")
def sshuttle(profile):
    """
    tool_xuan sshuttle <aws_profile>
    """
    sshuttle_devad = "sshuttle --dns -NHr bastion-devad 10.24.0.0/16"
    sshuttle_prodco = "sshuttle --dns -NHr bastion-prodco 10.22.0.0/16"
    if profile == "devad":
        system(sshuttle_devad)
    elif profile == "prodco":
        system(sshuttle_prodco)

    click.echo(click.style(f'You connect to {profile} VPN !', fg='green'))
