import click
import pyperclip


@click.command()
def aws_cicd():
    """
    tool_xuan cicd
    """
    pyperclip.copy("https://docs.aws.amazon.com/whitepapers/latest/practicing-continuous-integration-continuous-delivery/the-challenge-of-software-delivery.html")
    click.echo(click.style('Practicing Continuous Integration and Continuous Delivery on AWS !', fg='green'))


