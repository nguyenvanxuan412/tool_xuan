import boto3


def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)

