import boto3
from tool_check.utils.aws_profile_client import aws_profile_client
import click


@click.command()
@click.argument("profile")
def codebuild_xuan(profile):
    aws_profile = aws_profile_client(profile)
    codebuild = aws_profile.client("codebuild")
    response = codebuild.start_build(
        projectName='patient-referral-pr-xuan')
    print(f'{response["build"]["logs"]}')