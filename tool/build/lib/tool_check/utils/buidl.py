{
    'build': {
        'id': 'string',
        'arn': 'string',
        'buildNumber': 123,
        'startTime': datetime(2015, 1, 1),
        'endTime': datetime(2015, 1, 1),
        'currentPhase': 'string',
        'buildStatus': 'SUCCEEDED'|'FAILED'|'FAULT'|'TIMED_OUT'|'IN_PROGRESS'|'STOPPED',
        'sourceVersion': 'string',
        'resolvedSourceVersion': 'string',
        'projectName': 'string',
        'phases': [
            {
                'phaseType': 'SUBMITTED'|'QUEUED'|'PROVISIONING'|'DOWNLOAD_SOURCE'|'INSTALL'|'PRE_BUILD'|'BUILD'|'POST_BUILD'|'UPLOAD_ARTIFACTS'|'FINALIZING'|'COMPLETED',
                'phaseStatus': 'SUCCEEDED'|'FAILED'|'FAULT'|'TIMED_OUT'|'IN_PROGRESS'|'STOPPED',
                'startTime': datetime(2015, 1, 1),
                'endTime': datetime(2015, 1, 1),
                'durationInSeconds': 123,
                'contexts': [
                    {
                        'statusCode': 'string',
                        'message': 'string'
                    },
                ]
            },
        ],
        'source': {
            'type': 'CODECOMMIT'|'CODEPIPELINE'|'GITHUB'|'S3'|'BITBUCKET'|'GITHUB_ENTERPRISE'|'NO_SOURCE',
            'location': 'string',
            'gitCloneDepth': 123,
            'gitSubmodulesConfig': {
                'fetchSubmodules': True|False
            },
            'buildspec': 'string',
            'auth': {
                'type': 'OAUTH',
                'resource': 'string'
            },
            'reportBuildStatus': True|False,
            'buildStatusConfig': {
                'context': 'string',
                'targetUrl': 'string'
            },
            'insecureSsl': True|False,
            'sourceIdentifier': 'string'
        },
        'secondarySources': [
            {
                'type': 'CODECOMMIT'|'CODEPIPELINE'|'GITHUB'|'S3'|'BITBUCKET'|'GITHUB_ENTERPRISE'|'NO_SOURCE',
                'location': 'string',
                'gitCloneDepth': 123,
                'gitSubmodulesConfig': {
                    'fetchSubmodules': True|False
                },
                'buildspec': 'string',
                'auth': {
                    'type': 'OAUTH',
                    'resource': 'string'
                },
                'reportBuildStatus': True|False,
                'buildStatusConfig': {
                    'context': 'string',
                    'targetUrl': 'string'
                },
                'insecureSsl': True|False,
                'sourceIdentifier': 'string'
            },
        ],
        'secondarySourceVersions': [
            {
                'sourceIdentifier': 'string',
                'sourceVersion': 'string'
            },
        ],
        'artifacts': {
            'location': 'string',
            'sha256sum': 'string',
            'md5sum': 'string',
            'overrideArtifactName': True|False,
            'encryptionDisabled': True|False,
            'artifactIdentifier': 'string',
            'bucketOwnerAccess': 'NONE'|'READ_ONLY'|'FULL'
        },
        'secondaryArtifacts': [
            {
                'location': 'string',
                'sha256sum': 'string',
                'md5sum': 'string',
                'overrideArtifactName': True|False,
                'encryptionDisabled': True|False,
                'artifactIdentifier': 'string',
                'bucketOwnerAccess': 'NONE'|'READ_ONLY'|'FULL'
            },
        ],
        'cache': {
            'type': 'NO_CACHE'|'S3'|'LOCAL',
            'location': 'string',
            'modes': [
                'LOCAL_DOCKER_LAYER_CACHE'|'LOCAL_SOURCE_CACHE'|'LOCAL_CUSTOM_CACHE',
            ]
        },
        'environment': {
            'type': 'WINDOWS_CONTAINER'|'LINUX_CONTAINER'|'LINUX_GPU_CONTAINER'|'ARM_CONTAINER'|'WINDOWS_SERVER_2019_CONTAINER',
            'image': 'string',
            'computeType': 'BUILD_GENERAL1_SMALL'|'BUILD_GENERAL1_MEDIUM'|'BUILD_GENERAL1_LARGE'|'BUILD_GENERAL1_2XLARGE',
            'environmentVariables': [
                {
                    'name': 'string',
                    'value': 'string',
                    'type': 'PLAINTEXT'|'PARAMETER_STORE'|'SECRETS_MANAGER'
                },
            ],
            'privilegedMode': True|False,
            'certificate': 'string',
            'registryCredential': {
                'credential': 'string',
                'credentialProvider': 'SECRETS_MANAGER'
            },
            'imagePullCredentialsType': 'CODEBUILD'|'SERVICE_ROLE'
        },
        'serviceRole': 'string',
        'logs': {
            'groupName': 'string',
            'streamName': 'string',
            'deepLink': 'string',
            's3DeepLink': 'string',
            'cloudWatchLogsArn': 'string',
            's3LogsArn': 'string',
            'cloudWatchLogs': {
                'status': 'ENABLED'|'DISABLED',
                'groupName': 'string',
                'streamName': 'string'
            },
            's3Logs': {
                'status': 'ENABLED'|'DISABLED',
                'location': 'string',
                'encryptionDisabled': True|False,
                'bucketOwnerAccess': 'NONE'|'READ_ONLY'|'FULL'
            }
        },
        'timeoutInMinutes': 123,
        'queuedTimeoutInMinutes': 123,
        'buildComplete': True|False,
        'initiator': 'string',
        'vpcConfig': {
            'vpcId': 'string',
            'subnets': [
                'string',
            ],
            'securityGroupIds': [
                'string',
            ]
        },
        'networkInterface': {
            'subnetId': 'string',
            'networkInterfaceId': 'string'
        },
        'encryptionKey': 'string',
        'exportedEnvironmentVariables': [
            {
                'name': 'string',
                'value': 'string'
            },
        ],
        'reportArns': [
            'string',
        ],
        'fileSystemLocations': [
            {
                'type': 'EFS',
                'location': 'string',
                'mountPoint': 'string',
                'identifier': 'string',
                'mountOptions': 'string'
            },
        ],
        'debugSession': {
            'sessionEnabled': True|False,
            'sessionTarget': 'string'
        },
        'buildBatchArn': 'string'
    }
}