import subprocess
import shlex
import os


def run_cmd2(command, cwd=None, env=None, show=False, get_stderr=False):
    my_env = os.environ.copy()
    if env:
        my_env.update(env)

    extra = {}
    if get_stderr:
        extra["stderr"] = subprocess.STDOUT

    output = subprocess.check_output(
        shlex.split(command), shell=False, cwd=cwd, env=my_env, **extra
    ).decode("utf-8")

    if show:
        print(output)

    return output


# def run_cmd(command: str, cwd=None, show=True, check=False):
#     out_put = subprocess.Popen([command], shell=True, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
#     combined_output = out_put.stdout + out_put.stderr
#     if show:
#         print(combined_output)
#
#     return combined_output

# subprocess.Popen(
#             command_items,
#             # bufsize=1,
#             stdout=subprocess.PIPE,
#             stderr=subprocess.STDOUT,
#             shell=False,
#             cwd=cwd,
#             env=my_env,
#         )

def run_cmd(command: str, cwd=None, show=False, check=False):
    command_items = shlex.split(command)
    my_env = os.environ.copy()
    out_put = subprocess.run([command], shell=True, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                             text=True, env=my_env)
    combined_output = out_put.stdout
    if show:
        print(combined_output)

    return combined_output


def _run_cmd_an(command, **kwargs):
    output = (
        subprocess.check_output(shlex.split(command), **kwargs).decode("utf-8").strip()
    )
    return output


def warning_msg(msg):
    print(msg)


def check_cmd(command: str, cwd=None, show=True, check=False, shell=False):
    command_items = shlex.split(command)
    my_env = os.environ.copy()
    output = subprocess.Popen(command_items,
                              cwd=cwd,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,
                              text=True,
                              env=my_env)
    combined_output = output.stdout
    if show:
        print(combined_output)

    return combined_output
