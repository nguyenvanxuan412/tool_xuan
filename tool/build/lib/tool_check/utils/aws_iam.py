import boto3
import click
from click import echo


@click.group()
def cli():
    pass


@click.command()
@click.argument('profile', default="tenpoint7")
def iam(profile):
    """
    tool_xuan iam <aws_profile>
    """
    try:
        aws_profile = boto3.session.Session(profile_name=profile)
        iam_user = aws_profile.client("iam")
        list_user = iam_user.list_users()
        for user in list_user['Users']:
            echo(click.style(user['UserName'], fg='green', bold=True))
    except:
        echo(click.style(f'No Aws Profile {profile} in Credentials File: ', fg='red',bold=True))


@click.command()
@click.argument('user')
def group_for_user(user):
    """
    tool_xuan group <iam_user>
    """
    try:
        aws_profile = boto3.session.Session(profile_name="tenpoint7")
        iam_user = aws_profile.client("iam")

        groups = iam_user.list_groups_for_user(UserName=user)
        for group in groups['Groups']:
            echo(click.style(group.get('GroupName'), fg='cyan', bold=True))
    except:
        echo(click.style(f'No IAM User {user} in AWS Account ', fg='red',bold=True))


# cli.add_command(iam)
# cli.add_command(group_for_user, name='user')

# if __name__ == '__main__':
#     cli()
