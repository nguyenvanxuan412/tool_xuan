import click
from pathlib import Path


@click.command()
@click.argument('aws_profile')
def pr(aws_profile):
    if aws_profile == "devco":
        param = open(Path.cwd() / '/home/xuanit/practice/2_Practice/click/utils/core_param_devco')
        param = param.read()
        click.echo(click.style(param, fg='green'))
    elif aws_profile == "devad":
        param = open(Path.cwd() / '/home/xuanit/practice/2_Practice/click/utils/core_param_devad')
        param = param.read()
        # print(param)
        click.echo(click.style(param, fg='green'))

    else:
        click.echo(click.style('You Type Wrong AWS Profile !', fg='green'))


# if __name__ == '__main__':
#     pr()
