import click
import pyperclip


@click.command()
def aws_prescriptive():
    """
    tool_xuan awspr
    """
    pyperclip.copy("https://aws.amazon.com/prescriptive-guidance/?apg-all-cards.sort-by=item.additionalFields.sortDate&apg-all-cards.sort-order=desc&awsf.apg-new-filter=*all&awsf.apg-content-type-filter=*all&awsf.apg-code-filter=*all&awsf.apg-category-filter=categories%23devops&awsf.apg-rtype-filter=*all&awsf.apg-isv-filter=*all&awsf.apg-product-filter=*all&awsf.apg-env-filter=*all")
    click.echo(click.style('You Copy AWS Prescriptive Guidance !', fg='green'))


