from tool_check.utils.aws_profile_client import aws_profile_client
import boto3
import click


@click.command()
@click.argument('profile')
def call_lambda(profile):
    aws_profile = aws_profile_client(profile)
    lambda_call = aws_profile.client('lambda')
    responds = lambda_call.invoke(FunctionName="Lambda_Send_Email",)
    print(responds['Payload'].read().decode("utf-8"))