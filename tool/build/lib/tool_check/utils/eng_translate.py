# translator = Translator()
#
# dt1 = translator.detect(text1)
# print(dt1)
#
# dt2 = translator.detect(text2)
# print(dt2)

from googletrans import Translator
import click
from click import echo

import sys
from tool_check.utils.cmd_utils import run_cmd


def process_text(text):
    # Combine all the arguments into a single string
    text = ' '.join(text)

    # Process the text as needed
    # For now, let's just print it
    print(text)
    print()
    return text



@click.command()
@click.argument('text', nargs=-1, required=True)
def eng_transslate(text):

    full_arguments = process_text(text)

    translator = Translator()
    translated = translator.translate(f'{full_arguments}', dest='vi')

    # print(translated.text)
    click.echo(click.style(translated.text, fg='green'))
    run_cmd(command=f'echo "{full_arguments}" >> /home/xuanit/Desktop/3.Working/4.AWS/Prac2/new_eng')

