import click
import pyperclip


@click.command()
def devops():
    """
    tool_xuan devops
    """
    pyperclip.copy("https://docs.aws.amazon.com/wellarchitected/latest/devops-guidance/devops-guidance.html?did=wp_card&trk=wp_card")
    click.echo(click.style('AWS DevOps Guidance !', fg='green'))


