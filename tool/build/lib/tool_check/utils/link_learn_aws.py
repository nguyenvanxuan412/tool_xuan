import click

link_aws_learn = """"Exam guide: https://aws.amazon.com/certification/certification-prep/
Docs: https://docs.aws.amazon.com/
https://aws.amazon.com/getting-started/hands-on/
https://aws.amazon.com/whitepapers/
https://explore.skillbuilder.aws/learn
https://aws.amazon.com/architecture/
https://workshops.aws/
https://cloudjourney.awsstudygroup.com/vi/
https://www.youtube.com/playlist...
https://github.com/aws-samples
https://github.com/orgs/awslabs/repositories
Find labs by your self: + labs/workshops
https://www.udemy.com/user/stephane-maarek/
https://learn.cantrill.io/
https://cloudacademy.com/
https://acloudguru.com/
https://www.whizlabs.com/
https://app.pluralsight.com/library/
https://www.skillsoft.com/
https://www.techtarget.com/searchcloudcomputing/
"""

@click.command()
def link_aws():
    """
    show link to learn aws
    tool_xuan link
    """
    click.echo(click.style(link_aws_learn, fg='green'))