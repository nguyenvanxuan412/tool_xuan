import awscost
import click
import subprocess
import shlex


@click.command()
@click.option('--monitor', '-m', default="DAILY")
@click.option('--numbers', '-n', default=6)
@click.argument('aws_profile')
def cost(monitor, numbers, aws_profile):
    # click.echo(f'{monitor} {numbers} and {aws_profile}')
    # shlex.split(f'awscost -g {monitor} -p {numbers} -d SERVICE --aws-profile {aws_profile}')
    subprocess.run(shlex.split(f'awscost -g {monitor} -p {numbers} -d SERVICE --aws-profile {aws_profile}'))


@click.command()
@click.option('--monitor', '-m', default="MONTHLY")
@click.option('--numbers', '-n', default=4)
@click.argument('aws_profile')
def costmonthly(monitor, numbers, aws_profile):
    subprocess.run(shlex.split(f'awscost -g {monitor} -p {numbers} -d SERVICE --aws-profile {aws_profile}'))


@click.command()
@click.argument('monitor')
@click.argument('aws_profile')
def costspdate(monitor, aws_profile):
    """
    tool_xuan costspdate <year-month-date> <aws_account>
    """
    year, month, date = monitor.split("/")
    next_date = int(date) + 1
    if next_date < 10:
        next_date = "0" + str(next_date)
    else:
        next_date = str(next_date)
    subprocess.run(shlex.split(f'awscost --start "{year}-{month}-{date}" --end "{year}-{month}-{next_date}" -d SERVICE '
                               f'--aws-profile {aws_profile}'))
