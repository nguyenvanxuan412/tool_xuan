import boto3
import click
from pprint import pprint
from tool_check.utils.aws_profile_client import aws_profile_client
from os import system

DEVOPS_TRACK_BUCKET = "track_monitor_bucket"


def s3_list_bucket(profile):
    aws_profile = aws_profile_client(profile)
    s3 = aws_profile.client('s3')
    bucket_list = []
    response = s3.list_buckets()
    print(f'Existing buckets: {len(response["Buckets"])}')
    for bucket in response['Buckets']:
        bucket_list.append(bucket["Name"])

    return bucket_list


BUCKET = "dev-capital-one-tp7-ml-model"
def s3_size(profile):
    bucket_list = s3_list_bucket(profile)
    for bucket in bucket_list:
        command = f'aws s3 ls --summarize --human-readable --recursive s3://{bucket} --profile {profile}'
        system(command)






#s3_list_bucket("devco")

s3_size("devco")
