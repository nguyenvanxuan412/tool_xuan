import boto3
import datetime
import boto3
import click
from pprint import pprint
from tool_check.utils.aws_profile_client import aws_profile_client
import pytz
from pathlib import Path


ini_time_for_now = datetime.datetime.now(pytz.utc)

now = datetime.datetime.now()
StartTime=(now - datetime.timedelta(days=2)).isoformat()
EndTime = now.isoformat()
print(EndTime)
print(StartTime)

def s3_size(profile):
    aws_profile = aws_profile_client(profile)
    cw = aws_profile.client('cloudwatch')
    s3client = aws_profile.client('s3')

    # Get a list of all buckets
    allbuckets = s3client.list_buckets()

    # Header Line for the output going to standard out
    print('Bucket'.ljust(45) + 'Size in Bytes'.rjust(25))
    # print(allbuckets["Buckets"])

    # Iterate through each bucket
    for bucket in allbuckets['Buckets']:
        # For each bucket item, look up the cooresponding metrics from CloudWatch
        response = cw.get_metric_statistics(Namespace='AWS/S3',
                                            MetricName='BucketSizeBytes',
                                            Dimensions=[
                                                {'Name': 'BucketName', 'Value': bucket['Name']},
                                                {'Name': 'StorageType', 'Value': 'StandardStorage'}
                                            ],
                                            Statistics=['Average'],
                                            Period=3600,
                                            StartTime=(now - datetime.timedelta(days=2)).isoformat(),
                                            EndTime=now.isoformat()
                                            )
        # print(f'{bucket["Name"]} {response}')
        # The cloudwatch metrics will have the single datapoint, so we just report on it.
        for item in response["Datapoints"]:
            print(bucket["Name"].ljust(45) + str("{:,}".format(int(item["Average"]))).rjust(25))
            # Note the use of "{:,}".format.
            # This is a new shorthand method to format output.
            # I just discovered it recently.
            baconFile = open(f's3/s3_{ini_time_for_now.date()}', 'a')
            baconFile.write(f'{bucket["Name"]}   {item["Average"]}\n')
            baconFile.close()

s3_size("devco")
