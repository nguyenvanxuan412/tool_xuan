import boto3
from tool_check.utils.aws_profile_client import aws_profile_client

def size_s3(profile):
# Create an S3 client
    aws_profile = aws_profile_client(profile)
    s3 = aws_profile.client('s3')

    # Specify the bucket name
    bucket_name = 'dev-capital-one-tp7-ml-model'
    total_size = 0
    # Function to get the size of a bucket

        # Use paginator to list objects in the bucket, handling pagination automatically
    paginator = s3.get_paginator('list_objects_v2')
    for page in paginator.paginate(Bucket=bucket_name):
        for obj in page.get('Contents', []):
            total_size += obj['Size']

    print(total_size)

    # Convert the size to a human-readable format (e.g., MB, GB)
    # total_size_MB = total_size / (1024 ** 2)  # Bytes to Megabytes
    # total_size_GB = total_size / (1024 ** 3)  # Bytes to Gigabytes

    # return total_size, total_size_MB, total_size_GB

    # Get the size of the bucket


size_s3("devco")