import os

import click
from tool_check.utils.cmd_utils import run_cmd
import re
from click import echo
from os.path import exists
import boto3
from os import system

ip_address_pattern = re.compile(r'\b(?:\d{1,3}\.){3}\d{1,3}\b')

AWS_PROFILE = "devops"


def ec2_connect():
    aws_profile = boto3.session.Session(profile_name=AWS_PROFILE)
    ec2 = aws_profile.client('ec2')
    return ec2


@click.command()
def run_vpn_server():
    ec2 = ec2_connect()
    response = ec2.describe_instances(Filters=[{'Name': 'instance-state-name', 'Values': ['stopped']}])

    for reservation in response["Reservations"]:
        for instance in reservation["Instances"]:
            for tag in instance["Tags"]:
                if tag["Key"] == "Name":
                    if tag["Value"] == "openvpn":
                        ec2.start_instances(InstanceIds=[instance["InstanceId"]])


@click.command()
def vpn_connect():
    if exists("/tmp/session-path"):
        echo(click.style(f'VPN connected already', fg='green'))
        return

    file_read = "/home/xuanit/tool/tool_check/openvpn/xuannguyen.ovpn"

    public_ip = run_cmd(command="tool_xuan public devops/openvpn")
    ip_address = ip_address_pattern.search(public_ip)

    new_address = ip_address.group()

    f = open(file_read, 'r')
    lines = f.readlines()
    line_remote = lines[3]
    f.close()
    remote, old_public_ip, port = line_remote.split(" ")
    new_line = f"{remote} {new_address} {port}"
    print(new_line)
    with open(file_read, 'r', encoding='utf-8') as file:
        data = file.readlines()
    data[3] = new_line
    with open(file_read, 'w', encoding='utf-8') as file:
        file.writelines(data)
    command = f"openvpn3 session-start --config {file_read}"
    # system(command)
    out_put = run_cmd(command=command)
    with open("/tmp/session-path", 'w', encoding='utf-8') as file:
        file.writelines(out_put)

    echo(click.style(f'VPN connected {new_address}', fg='green'))


@click.command()
def vpn_disconnect():
    if exists("/tmp/session-path"):
        with open("/tmp/session-path", 'r', encoding='utf-8') as file:
            data = file.readlines()
        session_path = data[1]
        name, path, session = session_path.split(" ")
        command = f"openvpn3 session-manage --disconnect --session-path {session}"
        run_cmd(command=command, show=True)
        os.remove("/tmp/session-path")
    else:
        print("You did not connect VPN -  Please Conect VPN First")
