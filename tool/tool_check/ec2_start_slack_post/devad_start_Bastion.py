import boto3
from tool_check.ec2_start_slack_post.ec2_simple_request import request
from tool_check.ec2_start_slack_post.ec2_slack_post import response
import json

SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"
AWS_PROFILE = "devad"


def ec2_connect():
    aws_profile = boto3.session.Session(profile_name=AWS_PROFILE)
    ec2 = aws_profile.client('ec2')
    return ec2


def list_ec2_instance_running():
    ec_list_running = []
    count = 0
    ec2 = ec2_connect()
    response = ec2.describe_instances(Filters=[{'Name': 'instance-state-name', 'Values': ['stopped']}])

    for reservation in response["Reservations"]:
        for instance in reservation["Instances"]:
            count += 1
            for tag in instance["Tags"]:
                if tag["Key"] == "Name":
                    if tag["Value"] == "Bastion":
                        ec_list_running.append(f'{instance["InstanceId"]}: {tag["Value"]}')
                        ec2.start_instances(InstanceIds=[instance["InstanceId"]])

    blocks = [{
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": f'*{len(ec_list_running)} instance Starting*\n',

        },
    }]

    for i in ec_list_running:
        blocks.append(

            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f'{i} Running\n',

                },
            }
        )
    # print(blocks)
    return blocks


def running():
    data_need = request("EC2", AWS_PROFILE, list_ec2_instance_running())
    # print(data_need)
    response(slack_webhook_url=SLACK_WEBHOOK_URL, data=data_need)


if __name__ == '__main__':
    running()