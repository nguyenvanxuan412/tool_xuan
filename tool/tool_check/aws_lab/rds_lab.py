import click
import pyperclip


@click.command()
def rds_lab():
    """
    tool_xuan rdslab
    """
    pyperclip.copy("https://catalog.us-east-1.prod.workshops.aws/workshops/0135d1da-9f07-470c-9845-44ead3c78212/en-US")
    click.echo(click.style('You Copy AWS RDS LAB !', fg='green'))


