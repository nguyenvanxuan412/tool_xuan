import boto3
import click
from click import echo
from tool_check.utils.ec2_utils import ec2_id


def security_group(profile):
    aws_profile = boto3.session.Session(profile_name=profile)
    ec2 = aws_profile.client("ec2")
    tag_key = "Name"
    respond = ec2.describe_instances(
        Filters=[
            {
                'Name': 'instance-state-name',
                'Values': [
                    'running',
                ]

            },
            {
                'Name': f'tag:{tag_key}',
                'Values': [
                    'NATInstance',
                ],

            },
        ],
    )

    return respond["Reservations"][0]["Instances"][0]["SecurityGroups"][0].get("GroupId")


def _security_group_describe(profile):
    sg_id = security_group(profile)
    try:
        aws_profile = boto3.session.Session(profile_name=profile)
        ec2 = aws_profile.client("ec2")
        info = ec2.describe_security_groups(
            GroupIds=[
                sg_id,
            ],
        )
        for key in info["SecurityGroups"][0]["IpPermissions"]:
            echo(click.style(
                f'Protocol:{key["IpProtocol"]} - IP_Ranges: {key["IpRanges"]} - FromPort: {key.get("FromPort")} - ToPort: {key.get("ToPort")}\n',
                fg='green', bold=True))
    except:
        print(f'Security group describe Fail Please Check Profile or sg_id')


@click.group()
def cli():
    pass


@click.command()
@click.argument('profile')
@click.option('--on', 'transformation', flag_value='on',
              default=True)
@click.option('--off', 'transformation', flag_value='off')
def ssh(profile, transformation):
    """
    tool_xuan ssh <aws_profile> --on or --off
    """
    try:
        aws_profile = boto3.session.Session(profile_name=profile)
        ec2 = aws_profile.client("ec2")
        if transformation == "on":
            ec2.authorize_security_group_ingress(
                GroupId=security_group(profile),
                IpPermissions=[
                    {
                        'FromPort': 22,
                        'IpProtocol': 'tcp',
                        'IpRanges': [
                            {
                                'CidrIp': '0.0.0.0/0',
                                'Description': 'SSH'
                            },
                        ],
                        'ToPort': 22,
                    },
                ],
            )
            _security_group_describe(profile)
        elif transformation == "off":
            try:
                ec2.revoke_security_group_ingress(
                    GroupId=security_group(profile),
                    IpPermissions=[
                        {
                            'FromPort': 22,
                            'IpProtocol': 'tcp',
                            'IpRanges': [
                                {
                                    'CidrIp': '0.0.0.0/0',
                                    'Description': 'SSH'
                                },
                            ],
                            'ToPort': 22,
                        },
                    ],
                )
                _security_group_describe(profile)
            except:
                echo(click.style(f'Security Group Rule SSH not exist', fg='red'))

    except:
        echo(click.style(f'Security Group Rule SSH already exists or Wrong sg_id', fg='red'))
        _security_group_describe(profile)



cli.add_command(ssh)

if __name__ == '__main__':
    cli()
    # describe_nat_instace("devco")
