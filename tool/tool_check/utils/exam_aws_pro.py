import click
import pyperclip


@click.command()
def exam():
    """
    tool_xuan exam
    """
    pyperclip.copy("https://github.com/1davidmichael/AWS-Certified-Solutions-Architect-Professional-Exam-Study-Notes")
    click.echo(click.style('You Copy AWS Pro Note !', fg='green'))
