import click
from tool_check.ec2_start_slack_post.devad_start_Bastion import running


@click.command()
def start_bastion_addy():
    """
    tool_xuan Start Bastion DevAddy AWS Account
    """
    running()
    click.echo(click.style('You Start Bastion Host on devad account !', fg='green'))


if __name__ == '__main__':
    start_bastion_addy()