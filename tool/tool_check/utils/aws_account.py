import click

aws_account = {
    "prodco":       971387324083,
    "devco":        403691422752,
    "devad":        360500835193,
    "tenpoint7":    794620008521,
    "devops":       151212465715,
    "prodans":      753347736013,
    "devans":       706865936451,
    "log":          200618958799,
    "prodns":       "035731413964",
    "devns":        255866942588,
    "deviowa":      "043869264912",
    "prodiowa":     439354617523,
    "prodasui":     "058264483031",
    "devasui":      471112803118
}


@click.command()
def account_id():
    for account_name, id in aws_account.items():
        click.echo(click.style(f'{account_name} : {id}', fg='green'))