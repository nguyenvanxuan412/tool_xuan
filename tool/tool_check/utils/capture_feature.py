import click
from mss import mss
import datetime


@click.command()
@click.option('--monitor', '-m', default=2)
@click.argument('path')
@click.argument('aws_service')
def capture(monitor, path, aws_service):
    """
    tool_xuan capture -m <number> <path> <aws_service>

    """
    TIME = datetime.datetime.now()
    sct = mss()
    filename = sct.shot(mon=monitor, output=f'{path}/{aws_service}-{TIME}.png')
    # print(f'"You have capture monitor  {filename}')
    click.echo(f'{monitor} {aws_service} and {path}')