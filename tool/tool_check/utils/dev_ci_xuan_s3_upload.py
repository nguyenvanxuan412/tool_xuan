import boto3
import click

devops = boto3.session.Session(profile_name='devops')
s3 = devops.client('s3')




# s3.upload_file(FILE_LOCATION,BUCKET,KEY)

@click.command()
@click.argument("version")
def s3_ci(version):
    FILE_LOCATION = "/home/xuanit/practice/2_Practice/1.aws/ci_final/ci-xuan"
    BUCKET = 'xuan-test-ci'
    KEY = "ci-xuan"
    """
    tool_xuan s3_ci <version>
    """
    CI_NAME_VERSION = f'{KEY}-{version}'
    s3.upload_file(FILE_LOCATION, BUCKET, CI_NAME_VERSION)
    click.echo(click.style(f'You Uploaded ci-xuan-{version} to {BUCKET} bucket !', fg='green'))


@click.command()
@click.argument("version")
def s3_dev(version):
    FILE_LOCATION = "/home/xuanit/practice/2_Practice/1.aws/dev_final/dev-xuan"
    BUCKET = 'xuan-test-ci'
    KEY = "dev-xuan"
    """
    tool_xuan dev_ci <version>
    """
    CI_NAME_VERSION = f'{KEY}-{version}'
    s3.upload_file(FILE_LOCATION, BUCKET, CI_NAME_VERSION)
    click.echo(click.style(f'You Uploaded dev-xuan-{version} to {BUCKET} bucket !', fg='green'))
