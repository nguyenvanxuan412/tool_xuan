from googletrans import Translator
import click

from tool_check.utils.cmd_utils import run_cmd
from datetime import datetime


def process_text(text):
    # Combine all the arguments into a single string
    text = ' '.join(text)

    # Process the text as needed
    # For now, let's just print it
    print(text)
    print()
    return text



@click.command()
@click.argument('text', nargs=-1, required=True)
def daily_task(text):

    full_arguments = process_text(text)
    now = datetime.now()
    click.echo(click.style(f'{now} {full_arguments}', fg='green'))
    run_cmd(command=f'echo "{now} - {full_arguments}" >> /home/xuanit/Desktop/3.Working/task')