import boto3
import click
import rich
from os import system
from tool_check.utils.aws_profile_client import aws_profile_client
from click import echo
from pprint import pprint


def ec2_describe(profile_key):
    profile, key = profile_key.split('/')
    echo(click.style(f'AWS Profile: {profile} / Instance: {key}', fg='green'))
    with rich.get_console().status("Find instance by name..."):
        aws_profile = aws_profile_client(profile)
        connect = aws_profile.client('ec2')
        response = connect.describe_instances()

    return response


@click.command()
@click.argument('profile_key')
def ec2_send_cli(profile_key):
    """
    tool_xuan sendcli <aws_profile/nat>
    """
    profile, key = profile_key.split('/')
    instance_id = ec2_id(profile_key)
    echo(click.style(f'{instance_id}', fg='blue'))
    # Create a Boto3 SSM client
    aws_profile = aws_profile_client(profile)
    ssm_client = aws_profile.client('ssm')

    # Specify the EC2 instance ID you want to send the command to
    # instance_id = 'your-instance-id'

    # Specify the document name (an SSM document) that defines the command to run
    document_name = 'AWS-RunShellScript'

    # Specify the command to run on the instance
    command = 'sudo sysctl -w net.ipv4.ip_forward=1'

    # Define the parameters for the SSM document
    parameters = {
        'commands': [command],
    }

    # Send the command to the instance
    response = ssm_client.send_command(
        InstanceIds=[instance_id],
        DocumentName=document_name,
        Parameters=parameters,
    )
    echo(click.style(f'cli: "{command}" send to {instance_id}', fg='green'))


@click.command()
@click.argument('profile_key')
def ec2_public_ip(profile_key):
    """
    tool_xuan public <aws_profile/name_of_instance>
    """
    profile, key = profile_key.split('/')
    response = ec2_describe(profile_key)
    public_ip = [instance['PublicIpAddress'] for reservation in response['Reservations'] for instance in
                 reservation['Instances'] for tag in instance['Tags'] if
                 'PublicIpAddress' in instance and key in tag['Value'].lower()]
    echo(click.style(f'{public_ip[0]}', fg='green'))
    return public_ip[0]


def ec2_id(profile_key):
    profile, key = profile_key.split('/')
    response = ec2_describe(profile_key)
    try:
        # instance_ids = [instance['InstanceId'] for reservation in response['Reservations'] for instance in
        #                 reservation['Instances'] for tag in instance['Tags'] if
        #                 instance['State']['Name'] == 'running' and key in tag['Value'].lower()]
        instance_ids = [instance for reservation in response['Reservations'] for instance in
                        reservation['Instances']]
        list_instance = []
        for instance in instance_ids:
            tag = instance.get("Tags")
            if tag:
                for item in tag:
                    if key in item['Value'].lower() and instance['State']['Name'] == 'running':
                        list_instance.append(instance['InstanceId'])
                        return instance['InstanceId']
            else:
                continue
        if not list_instance:
            print(f"Do not have Tags in instance or instance not running")

    except:
        print("do not have Tags")
        raise


@click.command()
@click.argument('profile_ec2')
def ec2_connet(profile_ec2):
    profile, ec2_name = profile_ec2.split('/')
    target = ec2_id(profile_ec2)
    echo(click.style(f'Instance_ID: {target}', fg='blue'))
    command = f'aws ssm start-session --target {target} --profile {profile}'
    system(command)

