from pytube import YouTube
import click
from click import echo

SAVE_FOLDER="/home/xuanit/Desktop/YOUTUBE"

@click.command()
@click.argument("url")
def download_youtube(url):

# Replace 'your_video_url' with the actual URL of the YouTube video
    video_url = f'{url}'

    # Create a YouTube object
    yt = YouTube(video_url)

    # Get the stream with the highest resolution
    video_stream = yt.streams.get_highest_resolution()

    # Download the video to the current working directory
    video_stream.download(output_path=SAVE_FOLDER)

    # print(f"Download completed. Video saved to: {SAVE_FOLDER}")
    click.echo(click.style(f'Download completed. Video saved to: {SAVE_FOLDER}', fg='green'))
