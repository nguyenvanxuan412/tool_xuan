
import boto3
import click
from click import echo

def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)

@click.command()
@click.argument("awsprofile")
def add_secret_value_secret_app_gmail(awsprofile):
    secret_session = aws_profile_client(awsprofile)
    # Create a Secrets Manager client
    secrets_manager_client = secret_session.client('secretsmanager')

    # Specify the secret name and the string value you want to add
    secret_name = 'SECURITY_EMAIL_APP'
    secret_value = 'weka oohw ecoe iqks'

    # Create or update the secret with the specified string value
    response = secrets_manager_client.put_secret_value(
        SecretId=secret_name,
        SecretString=secret_value
    )

    # Print the ARN of the secret
    print(f"Secret ARN: {response['ARN']}")

add_secret_value_secret_app_gmail()
