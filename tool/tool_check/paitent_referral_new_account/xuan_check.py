from pathlib import Path
from os.path import exists, join, expanduser
import click
from click import echo

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}projects/devops/data/account"


def aws_profile_add(aws_profile, account_id):
    aws_profile_path = f"{HOME}.aws/credentials"
    print(aws_profile_path)
    account = f"\n[{aws_profile}]\n"
    role_arn = f"role_arn = arn:aws:iam::{account_id}:role/OrganizationAccountAccessRole\n"
    source_profile = "source_profile = default\n"
    region = "region=us-west-2\n"
    with open(aws_profile_path, "r") as aws_profile:
        for line in aws_profile:
            # Strip whitespace from the beginning and end of the line
            line = line.strip()
            print(line)

aws_profile_add("deviowa", "4343434242")