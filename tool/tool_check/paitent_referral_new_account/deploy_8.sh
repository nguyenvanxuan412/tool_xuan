#!/bin/bash

# Define a list of strings
string_list=(
"-target=module.account.module.nat-instance.module.vpc_data.data.aws_subnet.private_subnet_1"
"-target=module.account.module.nat-instance.module.vpc_data.data.aws_subnet.private_subnet_2"
"-target=module.account.module.nat-instance.module.vpc_data.data.aws_subnet.public_subnet_1"
"-target=module.account.module.nat-instance.module.vpc_data.data.aws_subnet.public_subnet_2"
"-target=module.account.module.nat-instance.module.vpc_data.data.aws_vpc.default"
"-target=module.account.module.nat-instance.data.aws_ami.ama-zone[0]"
"-target=module.account.module.nat-instance.aws_iam_instance_profile.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_iam_role.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_iam_role_policy_attachment.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_instance.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_route.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_route_table.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_route_table_association.nat_instance[0]"
"-target=module.account.module.nat-instance.aws_route_table_association.nat_instance[1]"
"-target=module.account.module.nat-instance.aws_security_group.nat_instance[0]"
)

if [ "$#" -lt 1 ]; then
  echo "Error: At least one argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"
d tf acc:$arg1 -- init && d tf acc:$arg1 -- apply "${string_list[@]}"