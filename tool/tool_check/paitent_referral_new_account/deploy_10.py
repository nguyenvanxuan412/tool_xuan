import os
import click
from os import system
from os.path import exists,expanduser
import subprocess

HOME = expanduser("~/")
CODE_FOLDER = f"{HOME}projects/patient-referral"
CODE_FOLDER_DEPLOY = f"{HOME}projects/patient-referral-deploy"

@click.command()
@click.argument("origin_branch")
@click.argument("new_branch")
def git_create_branch(origin_branch, new_branch):
    check_out_branch = f"git checkout {origin_branch}"
    git_pull = "git pull"
    create_new_branch = f"git checkout -b {new_branch}"
    os.chdir(CODE_FOLDER)
    subprocess.run(check_out_branch, shell=True)
    subprocess.run(git_pull, shell=True)
    subprocess.run(create_new_branch, shell=True)

    os.chdir(CODE_FOLDER_DEPLOY)
    subprocess.run(check_out_branch, shell=True)
    subprocess.run(git_pull, shell=True)
    subprocess.run(create_new_branch, shell=True)




git_create_branch()