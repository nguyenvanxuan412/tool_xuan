#!/bin/bash

# Define a list of strings
string_list=(
"-target=module.account.module.vpc[0].data.aws_iam_policy_document.flow_log_cloudwatch_assume_role[0]"
"-target=module.account.module.vpc[0].data.aws_iam_policy_document.vpc_flow_log_cloudwatch[0]"
"-target=module.account.module.vpc[0].aws_cloudwatch_log_group.flow_log[0]"
"-target=module.account.module.vpc[0].aws_flow_log.this[0]"
"-target=module.account.module.vpc[0].aws_iam_policy.vpc_flow_log_cloudwatch[0]"
"-target=module.account.module.vpc[0].aws_iam_role.vpc_flow_log_cloudwatch[0]"
"-target=module.account.module.vpc[0].aws_iam_role_policy_attachment.vpc_flow_log_cloudwatch[0]"
"-target=module.account.module.vpc[0].aws_internet_gateway.this[0]"
"-target=module.account.module.vpc[0].aws_route.public_internet_gateway[0]"
"-target=module.account.module.vpc[0].aws_route_table.private[0]"
"-target=module.account.module.vpc[0].aws_route_table.private[1]"
"-target=module.account.module.vpc[0].aws_route_table.public[0]"
"-target=module.account.module.vpc[0].aws_route_table_association.public[0]"
"-target=module.account.module.vpc[0].aws_route_table_association.public[1]"
"-target=module.account.module.vpc[0].aws_subnet.private[0]"
"-target=module.account.module.vpc[0].aws_subnet.private[1]"
"-target=module.account.module.vpc[0].aws_subnet.public[0]"
"-target=module.account.module.vpc[0].aws_subnet.public[1]"
"-target=module.account.module.vpc[0].aws_vpc.this[0]"

)

if [ "$#" -lt 1 ]; then
  echo "Error: At least one argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"

d tf acc:$arg1 -- apply "${string_list[@]}"



