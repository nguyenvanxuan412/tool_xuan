#!/bin/bash



if [ "$#" -lt 2 ]; then
  echo "Error: At least two argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi
arg2="$2"

# Define a list of strings
string_list=(
"-target=module.infra.aws_ssm_parameter.capacity_provider[\"${arg2}\"]"
"-target=aws_ssm_parameter.api_app_host[\"${arg2}\"]"
"-target=aws_ssm_parameter.project_code[\"${arg2}\"]"
"-target=module.infra.aws_ssm_parameter.disable_components[\"${arg2}\"]"
"-target=module.infra.module.codebuild.aws_codebuild_project.all[\"${arg2}\"]"
"-target=module.infra.module.codebuild.aws_codebuild_webhook.all[\"${arg2}\"]"


)

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"
d tf $arg1/patient-referral -- apply "${string_list[@]}"