from pathlib import Path

# Specify the file path using Path
file_path = Path('pr_infra_new_account_old.txt')
output_file_path = Path('pr_infra_new_account.txt')
# Open the file in read mode
with file_path.open('r') as input_file, output_file_path.open('w') as output_file:
    # Iterate through each line in the file
    for line in input_file:
        # Process each line as needed
        modified_line = line.replace(line, f'"-target={line.strip()}"')
        print(modified_line.strip())
        output_file.write(modified_line.rstrip('\n') + '\n')
