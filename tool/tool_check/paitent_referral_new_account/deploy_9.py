import os

import boto3
import click
from os.path import exists,expanduser,join

HOME = expanduser("~/")
infra_path =  f"{HOME}projects/patient-referral-infra/terraform/config"

def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)
# Set your AWS credentials and region



@click.command()
@click.argument("state")
@click.argument("client")
@click.argument("branch")
def create_client_pr(state, client, branch):
    config_file_yaml = f"""
default:
  ecs_min_capacity: 1
  ecs_max_capacity: 1

  sub_env_prefix: pr-
  capacity_provider: FARGATE_SPOT
  deploy_branch: main

pr-{branch}:
  owner_name: Team
  branch_pattern: feature/pr-{branch}
  capacity_provider: FARGATE_SPOT
  deploy_branch: feature/{branch}
    """
    new_client_folder = f"{infra_path}/{client.upper()}"
    if exists(new_client_folder):
        print(new_client_folder)
        print("ok")
        if exists(f"{new_client_folder}/{state}_sub_env.yml"):
            print(f"The File {new_client_folder}/{state}_sub_env.yml exists.")
        else:
            with open(f"{new_client_folder}/{state}_sub_env.yml", "w") as config_file:
                config_file.write(config_file_yaml)
    else:
        print(f"The Folder '{new_client_folder}' does not exist.")
        os.makedirs(new_client_folder)
        with open(f"{new_client_folder}/{state}_sub_env.yml", "w") as config_file:
            config_file.write(config_file_yaml)
        print(f"Create {new_client_folder}/{state}_sub_env.yml")

create_client_pr()
