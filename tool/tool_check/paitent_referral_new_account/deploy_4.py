from os.path import expanduser, exists
import click
from click import echo

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}projects/devops/infra/acc"

infra_aws_alert_content = """
module "aws_alert" {
  source = "../../../tf/modules/aws_alert"
  acc_config = local.acc_config
}
"""

infra_backup_para_content = """
locals {

  backup = {"role":{"arn":"arn:aws:iam::${local.acc_config["id"]}:role/Backup","name":"Backup"},"vault_name":"Default"}
}

resource "aws_ssm_parameter" "config" {
  name  = "/Core/BACKUP"
  type  = "String"
  value = jsonencode(local.backup)
}
"""

infra_iam_content = """
module "iam_role" {
  source = "../../../tf/modules/patient-referral-new-client/iam"
  providers = {
    aws     = aws
    aws.tp7 = aws.tenpoint7
  }
  acc_config = local.acc_config
}
"""

infra_secret_content = """
resource "aws_secretsmanager_secret" "security_email_app" {
  name = "SECURITY_EMAIL_APP"
}

resource "aws_secretsmanager_secret" "bitbucket_api_key" {
  name = "BITBUCKET_API_KEY"
}

resource "aws_secretsmanager_secret" "ci_slack_incoming_web_hook_url" {
  name = "CI_SLACK_INCOMING_WEB_HOOK_URL"
}
"""

infra_slack_post_content = """
module "slack_post" {
  source = "../../../tf/modules/slack-post"

  secret_ci_slack_incoming_web_hook_url_arn = aws_secretsmanager_secret.ci_slack_incoming_web_hook_url.arn
}
"""


@click.command()
@click.argument("profile")
def create_infra_file(profile):
    aws_profile_path = f"{DATA_FOLDER}/{profile}"
    print(aws_profile_path)
    with open(f"{aws_profile_path}/infra_aws_alert.tf", "w") as infra_alert:
        infra_alert.write(infra_aws_alert_content)

    with open(f"{aws_profile_path}/infra_backup_para.tf", "w") as infra_alert:
        infra_alert.write(infra_backup_para_content)

    with open(f"{aws_profile_path}/infra_iam.tf", "w") as infra_alert:
        infra_alert.write(infra_iam_content)

    with open(f"{aws_profile_path}/infra_secret.tf", "w") as infra_alert:
        infra_alert.write(infra_secret_content)

    with open(f"{aws_profile_path}/infra_slack_post.tf", "w") as infra_alert:
        infra_alert.write(infra_slack_post_content)




create_infra_file()
