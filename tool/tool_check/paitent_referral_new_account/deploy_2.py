from pathlib import Path
from os.path import exists, join, expanduser
import click
from click import echo

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}projects/devops/data/account"


def validate_account_id(ctx, param, value):
    try:
        # Try to convert the value to an integer
        int(value)
        return value
    except ValueError:
        # If conversion to int fails, raise a BadParameter
        raise click.BadParameter(f"{value} is not a valid account_id. Please provide a number.")


@click.command()
@click.argument("stage")
@click.argument("client")
@click.argument("account_id", callback=validate_account_id)
@click.argument("cidr")
def create_aws_data_yml(stage, client, account_id, cidr):
    aws_profile_string = f"{stage}{client}"

    private_subnet_1 = ".".join([str(cidr[:5]), "0.0/18"])
    private_subnet_2 = ".".join([str(cidr[:5]), "64.0/18"])

    public_subnet_1 = ".".join([str(cidr[:5]), "128.0/18"])
    public_subnet_2 = ".".join([str(cidr[:5]), "192.0/18"])

    print(private_subnet_1)
    print(private_subnet_2)
    print(public_subnet_1)
    print(public_subnet_2)
    filepath = Path(f"{DATA_FOLDER}/{stage}{client}.yml")
    new_profile_content = f"""
id: {account_id}
title: {stage.title()}{client.upper()}
client: {client.upper()}
stage: {stage}
backup: false
create_common_security_group: true
create_fargate_cluster: true
create_tf_dynamodb_backend: true
create_nat_instance:
  create: true

vpc:
  create: true
  nat_instance: true
  cidr: {cidr}
  private_subnets:
    # https://www.ipaddressguide.com/cidr
    - {private_subnet_1}
    - {private_subnet_2}
  public_subnets:
    - {public_subnet_1}
    - {public_subnet_2}
s3:
  build:
    name: tp7-client-{client}-{stage}-build
    rule:
      - id: auto
        transitions:
          - days: 0
            storage_class: INTELLIGENT_TIERING
    policy:
      Version: "2012-10-17"
      Id: LambdaCrossAccess
      Statement:
        - Sid: ListBucket
          Effect: Allow
          Principal:
            AWS: arn:aws:iam::151212465715:role/OrganizationAccountAccessRole
          Action:
            - s3:ListBucket
          Resource: arn:aws:s3:::tp7-client-{client}-{stage}-build
        - Sid: CopyObject
          Effect: Allow
          Principal:
            AWS: arn:aws:iam::151212465715:role/OrganizationAccountAccessRole
          Action:
            - s3:PutObject
            - s3:GetObject
          Resource: arn:aws:s3:::tp7-client-{client}-{stage}-build/*
  devops:
    name: tp7-client-{client}-{stage}-devops
    version: yes
    rule:
      - id: auto
        transitions:
          - days: 0
            storage_class: INTELLIGENT_TIERING
  log:
    name: tp7-client-{client}-{stage}-log
    rule:
      - id: log-dev
        transitions:
          - days: 30
            storage_class: STANDARD_IA
        # Remove file after 1 year
        expiration_days: 365
        
tp7_pr_group: true

event_pattern:
  source:
    - aws.s3
    - aws.ec2
    - aws.events
    - aws.lambda
    - aws.ecs
    - aws.rds
  eventSource:
    - s3.amazonaws.com
    - ec2.amazonaws.com
    - events.amazonaws.com
    - lambda.amazonaws.com
    - ecs.amazonaws.com
    - rds.amazonaws.com
  eventName:
    - CreateBucket
    - DeleteBucket
    - DisableRule
    - EnableRule
    - PutRule
    - CreateFunction20150331
    - DeleteFunction20150331
    - CreateSecurityGroup
    - DeleteSecurityGroup
    - RunInstances
    - StopInstances
    - StartInstances
    - TerminateInstances
    - SERVICE_DISCOVERY_INSTANCE_UNHEALTHY
    - CreateDBInstance
    - DeleteDBInstance
    - ModifyDBInstance
    - DeleteCluster
    - CreateService
    - CreateCluster
  ecs_sv_name:
    - addy-test_web
    - addy-test_api_public
    - addy-test_api_app
    - addy-test_api_private
    - sv-test_api
"""
    if exists(filepath):
        click.echo(click.style(f'Client Existing', fg='green'))
        return
    else:
        with filepath.open('w') as output_file:
            output_file.write(new_profile_content)

        aws_profile_add(aws_profile=aws_profile_string, account_id=account_id)
        remove_first_line(filepath)


def remove_first_line(filepath):
    # Read the existing content
    filepath = filepath
    with filepath.open(mode='r') as file:
        lines = file.readlines()

    # Remove the first line
    lines = lines[1:]
    with filepath.open(mode='w') as file:
        file.writelines(lines)


def aws_profile_add(aws_profile, account_id):
    aws_profile_path = f"{HOME}.aws/credentials"
    print(aws_profile_path)
    account = f"\n[{aws_profile}]\n"
    role_arn = f"role_arn = arn:aws:iam::{account_id}:role/OrganizationAccountAccessRole\n"
    source_profile = "source_profile = default\n"
    region = "region=us-west-2\n"
    with open(aws_profile_path, "a") as new_aws_profile:
        new_aws_profile.write(account)
        new_aws_profile.write(role_arn)
        new_aws_profile.write(source_profile)
        new_aws_profile.write(region)


if __name__ == '__main__':
    create_aws_data_yml()
