#!/bin/bash


tft_target="d tft /home/xuanit/projects/devops/infra/devops/common.tf"
output_variable=$(bash -c "$tft_target")

# Print or use the captured output
echo "Output: $output_variable"

/home/xuanit/projects/tools/tools/bin/d tf devops -- init
/home/xuanit/projects/tools/tools/bin/d tf devops -- apply -target=module.s3