from pathlib import Path
from os.path import exists, join, expanduser
import click
from click import echo
import yaml
import json
import ast

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}projects/devops/data/account"
YAMl_FILE = f"{DATA_FOLDER}/tenpoint7.yml"
output_new_file = DATA_FOLDER

file_path = YAMl_FILE


@click.command()
@click.argument("ou")
def write_yml_file(ou):
    output_new_file = f"{DATA_FOLDER}/tenpoint7.yml"
    with open(YAMl_FILE, "r") as first_file:
        data = yaml.safe_load(first_file)
        print(type(data))

        data["organizations"]["ou_name"].append(ou)

        print()
        prod_account = ou
        prod_account_dict = f'"name": "tp7-{ou}-prod","email": "aws.client.{ou}@tenpoint7.com", "add_ou": "{ou}"'
        prod_account_dict_dict = ast.literal_eval("{" + prod_account_dict + "}")
        dev_account = f"{ou}_dev"
        dev_account_dict = f'"name": "tp7-{ou}-dev","email": "aws.dev.{ou}@tenpoint7.com", "add_ou": "{ou}"'
        dev_account_dict_dict = ast.literal_eval("{" + dev_account_dict + "}")

        data["organizations"]["account"][prod_account] = prod_account_dict_dict
        data["organizations"]["account"][dev_account] = dev_account_dict_dict
    # Writing a new yaml file with the modifications
    with open(output_new_file, "w") as new_file:
        yaml.dump(data, new_file, sort_keys=False)


