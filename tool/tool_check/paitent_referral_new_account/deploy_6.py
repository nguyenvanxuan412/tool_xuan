import boto3
import click
from click import echo


def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)


@click.command()
@click.argument("awsprofile")
def add_secret_value_secret_app_gmail(awsprofile):
    secret_session = aws_profile_client(awsprofile)
    # Create a Secrets Manager client
    secrets_manager_client = secret_session.client('secretsmanager')

    # Specify the secret name and the string value you want to add
    secret_name_bitbutkcet = 'BITBUCKET_API_KEY'
    secret_value_bitbucket = 'HjYnjcPnb59X4Cjx3H:mcCfM6QLDdsHmsv7agUsavPWFHpQBV29'

    # Create or update the secret with the specified string value
    response = secrets_manager_client.put_secret_value(
        SecretId=secret_name_bitbutkcet,
        SecretString=secret_value_bitbucket
    )

    secret_name_slack_hook = 'CI_SLACK_INCOMING_WEB_HOOK_URL'
    secret_value_slack_hook = 'https://hooks.slack.com/services/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'

    response1 = secrets_manager_client.put_secret_value(
        SecretId=secret_name_slack_hook,
        SecretString=secret_value_slack_hook
    )

    # Print the ARN of the secret
    print(f"Secret ARN: {response['ARN']}")
    print(f"Secret ARN: {response1['ARN']}")

add_secret_value_secret_app_gmail()