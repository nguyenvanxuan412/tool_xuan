import boto3
import click
from click import echo

secret_dict = {
    'UPDOX_CREDENTIALS': '{"username":"Jsong","password":"Neospine1234!"}',
    'IOWA_CREDENTIALS': '{"username":"Jsong","password":"Tenpoint7(IowaENT)!","url":"https://iowaent.prognocis.com"}',
    'NEOSPINE_PROGNOCIS_CREDENTIALS': '{"username":"Jsong","password":"Tenpoint7(Neospine)!"}',
    'PRONOCIS_USERNAME': 'TesDummy',
    'PRONOCIS_PASSWORD': '6hEUYTGTx593gLX',
    'PR_SENTRY_DSN': 'https://fb4dc80d33b2470c9340c1b356bacffb@o4504211752550400.ingest.sentry.io/4504971615272960',
    'AZURE_OPENAPI_KEY': '42b3bf8c4f8d40899240ec59d7005ffa',
    'ANESIS_GMAIL_SECRET': '{"ANESIS_SCOPES":"[\\"https://www.googleapis.com/auth/gmail.readonly\\"]",'
                           '"ANESIS_CLIENT_ID":"1002074107159-6sm76ms5vgt7c2mt4vgvikcc3549g9m7.apps.googleusercontent'
                           '.com","ANESIS_CLIENT_SECRET":"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",'
                           '"ANESIS_PROJECT_ID":"addy-ai",'
                           '"ANESIS_ACCESS_TOKEN":"ya29.a0AfB_byDZBv0YqbGcc6JG'
                           '-jHSqT3lW8jVwhSfUWrsBVWIT5WAmiow0kaO7_8r8PnXKsStrDU89gUqGzivKNThltJSDitro3D2F_NgyNYbf1utFlFNGVuSOOJNdyw-aZRLQHw7qas9bSU0REoHiRUTRzay4oSWusoWZzKcaCgYKAWISARASFQHGX2MiPJyCmvRqfFg4IARCEDDpoA0171","ANESIS_REFRESH_TOKEN":"1//04ExKN5QKQq1uCgYIARAAGAQSNwF-L9Ir7eAM2B5ryF7isERwkoSu4RspoqbH6BU8yIA9ZLv0pdDlfIP24th4VZqXMpYrXY5qBCo","ANESIS_TOKEN_URI":"https://oauth2.googleapis.com/token"}',
    'ECW_CREDENTIALS': '{"username":"tenpoint7","password":"H@ppyT!m3zzzzz"}',
    'GPT_API_KEY': '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',
    'AZURE_DOCUMENT_INTELLIGENCE_KEY': 'f7292597dbff4c0fb597fe4e5a07c308',
    'ASUI_SHAREPOINT_CREDENTIALS': '{"site_url":"https://asuicc.sharepoint.com/sites/ASUI2",'
                                    '"username":"addai@asui.org",'
                                    '"password":"Zuv83790",'
                                    '"folder_server_relative_url":"/sites/ASUI2/Shared Documents/INBOUND FAXES/REFERRALS"}'

}


def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)


@click.command()
@click.argument("awsprofile")
def add_secret_value_secret_app_gmail(awsprofile):
    secret_session = aws_profile_client(awsprofile)
    # Create a Secrets Manager client
    secrets_manager_client = secret_session.client('secretsmanager')

    # Specify the secret name and the string value you want to add

    for secret_name, secret_value in secret_dict.items():
        print(f"{secret_name} : {secret_value}")

        # Create or update the secret with the specified string value
        response = secrets_manager_client.put_secret_value(
            SecretId=secret_name,
            SecretString=secret_value
        )

        # Print the ARN of the secret
        print(f"Secret ARN: {response['ARN']}")


add_secret_value_secret_app_gmail()
