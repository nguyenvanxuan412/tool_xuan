
infra_module = [
"data.archive_file.lambda_trigger_raw_email",
"data.aws_dynamodb_table.patient_referral_fax_id",
"data.aws_secretsmanager_secret.lambda_trigger_raw_email",
"aws_cloudwatch_event_rule.lambda_trigger_raw_email",
"aws_cloudwatch_event_target.lambda_trigger_raw_email",

"aws_dynamodb_table.patient_referral_document", 1
"aws_dynamodb_table.patient_referral_processed_faxes",2

"aws_lambda_function.lambda_trigger_raw_email",
"aws_lambda_permission.lambda_trigger_raw_email",

"aws_secretsmanager_secret.ecw_credentials",3
"aws_secretsmanager_secret.iowa_credentials",4
"aws_secretsmanager_secret.neospine_prognocis_credentials",5
"aws_secretsmanager_secret.patient_referral_anesis_gmail_secret",6
"aws_secretsmanager_secret.patient_referral_azure_document_intelligence_key",7
"aws_secretsmanager_secret.patient_referral_azure_open_api_key",8
"aws_secretsmanager_secret.patient_referral_gpt_api_key",9
"aws_secretsmanager_secret.patient_referral_pronocis_password",10
"aws_secretsmanager_secret.patient_referral_pronocis_username",11
"aws_secretsmanager_secret.pr_sentry_dns",12
"aws_secretsmanager_secret.up_dox_credentials",13


"module.infra.data.aws_caller_identity.current",
"module.infra.data.aws_cloudfront_cache_policy.caching_disabled",
"module.infra.data.aws_cloudfront_origin_request_policy.all_viewer",
"module.infra.data.aws_iam_role.VNDev",
"module.infra.data.aws_route53_zone.tenpoint7",
"module.infra.data.aws_security_group.AllowAll",
"module.infra.data.aws_security_group.NoInAllOut",
"module.infra.data.aws_security_group.VPCInternal",
"module.infra.data.aws_security_group.Web",


"module.infra.aws_ssm_parameter.config", 14

"module.lambda_role_TriggerPrPipeline.aws_iam_role.lambda",15
"module.lambda_role_TriggerPrPipeline.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",16
"module.lambda_role_TriggerPrPipeline.aws_iam_role_policy_attachment.tracing[0]",17
"module.lambda_role_gmail_download.aws_iam_role.lambda",18
"module.lambda_role_gmail_download.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",19
"module.lambda_role_gmail_download.aws_iam_role_policy_attachment.tracing[0]",20
"module.lambda_role_raw_email.aws_iam_role.lambda",21
"module.lambda_role_raw_email.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",22
"module.lambda_role_raw_email.aws_iam_role_policy_attachment.tracing[0]",23
"module.lambda_role_rpa_prognocis.aws_iam_role.lambda",24
"module.lambda_role_rpa_prognocis.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",25
"module.lambda_role_rpa_prognocis.aws_iam_role_policy_attachment.tracing[0]",26
"module.lambda_role_rpa_trigger.aws_iam_role.lambda",27
"module.lambda_role_rpa_trigger.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",28


"module.lambda_role_trigger_raw_email.aws_iam_role.lambda",
"module.lambda_role_trigger_raw_email.aws_iam_role_policy_attachment.lambda_vpc_access_execution_role",
"module.lambda_role_trigger_raw_email.aws_iam_role_policy_attachment.tracing[0]",


"module.infra.module.codebuild.data.aws_s3_bucket.build",
"module.infra.module.codebuild.data.aws_s3_bucket.devops",
"module.infra.module.codebuild.data.aws_secretsmanager_secret.bitbucket_api_key",
"module.infra.module.codebuild.data.aws_security_group.NoInAllOut",
"module.infra.module.codebuild.data.aws_ssm_parameter.devops_core_s3_build_bucket",


"module.infra.module.codebuild.aws_iam_policy.codebuild_deploy_lambda[0]",29
"module.infra.module.codebuild.aws_iam_policy.project_deploy",30
"module.infra.module.codebuild.aws_iam_policy.project_deploy_p2",31
"module.infra.module.codebuild.aws_iam_role.codebuild",32
"module.infra.module.codebuild.aws_iam_role_policy_attachment.codebuild_deploy",33
"module.infra.module.codebuild.aws_iam_role_policy_attachment.codebuild_deploy_lambda[0]",34
"module.infra.module.codebuild.aws_iam_role_policy_attachment.codebuild_deploy_p2",35
"module.infra.module.codebuild.aws_security_group.codebuild",36
"module.infra.module.codebuild.aws_security_group_rule.codebuild_internet_access",37
]




target = "-target="
infra_module_list = []
for i in infra_module:
    i = f'{target}{i}'
    # print(i)
    infra_module_list.append(i)

print(*infra_module_list)




