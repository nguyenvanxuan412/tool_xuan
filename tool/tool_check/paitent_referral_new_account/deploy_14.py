
from pathlib import Path
from os.path import exists, join, expanduser
import click
from click import echo
import yaml
import json
import ast

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}projects/devops/data/project"
YAMl_FILE = f"{DATA_FOLDER}/project_account.yml"
output_new_file = DATA_FOLDER

file_path = YAMl_FILE


@click.command()
@click.argument("awsprofile")
def write_yml_file(awsprofile):
    output_new_file = f"{DATA_FOLDER}/project_account.yml"
    with open(YAMl_FILE, "r") as first_file:
        data = yaml.safe_load(first_file)
        print(type(data))
        data[awsprofile] = ["patient-referral"]


    # Writing a new yaml file with the modifications
    print(output_new_file)
    with open(output_new_file, "w") as new_file:
        yaml.dump(data, new_file, sort_keys=False)


write_yml_file()