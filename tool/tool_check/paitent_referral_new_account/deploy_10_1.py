from pathlib import Path
from os.path import exists, join, expanduser
import click
from click import echo
import yaml
import json
import ast

HOME = expanduser("~/")
DATA_FOLDER = f"{HOME}.config/dev"
YAMl_FILE = f"{DATA_FOLDER}/config.yml"
output_new_file = DATA_FOLDER

file_path = YAMl_FILE


@click.command()
@click.argument("awsprofile")
def write_yml_file(awsprofile):
    stage, client = awsprofile.split("/")
    output_new_file = YAMl_FILE
    with open(YAMl_FILE, "r") as first_file:
        data = yaml.safe_load(first_file)
        print(type(data))
        print()
        prod_account = f"{stage}{client}"
        prod_account_dict = f'"stage": "{stage}","client": "{client.upper()}"'
        prod_account_dict_dict = ast.literal_eval("{" + prod_account_dict + "}")

        data[".settings"]["aws_profile"][prod_account] = prod_account_dict_dict
    # Writing a new yaml file with the modifications
    with open(output_new_file, "w") as new_file:
        yaml.dump(data, new_file, sort_keys=False)

write_yml_file()

