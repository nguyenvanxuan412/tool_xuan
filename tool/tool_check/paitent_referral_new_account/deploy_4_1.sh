#!/bin/bash

# Define a list of strings
string_list=(
"-target=aws_secretsmanager_secret.security_email_app"

)

if [ "$#" -lt 1 ]; then
  echo "Error: At least one argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"

d tf acc:$arg1 -- init && d tf acc:$arg1 -- apply "${string_list[@]}"




