import os

import boto3
import click
from os.path import exists,expanduser,join

HOME = expanduser("~/")


def aws_profile_client(profile):
    return boto3.session.Session(profile_name=profile)
# Set your AWS credentials and region

@click.command()
@click.argument("awsprofile")
def create_key_pem_ec2(awsprofile):
    ec2_session = aws_profile_client(awsprofile)

    # Create a Boto3 EC2 client
    ec2 = ec2_session.client('ec2')

    # Key pair name
    key_pair_name = 'nat-instance'

    # Create key pair
    #response = ec2.create_key_pair(KeyName=key_pair_name)
    key_pair_path = F"{HOME}Desktop/3.Working/12.Nat_instance/{awsprofile}"
    if exists(key_pair_path):
        response = ec2.create_key_pair(KeyName=key_pair_name)
        # Save the private key to a file
        private_key_path = f'{key_pair_path}/{key_pair_name}.pem'
        with open(private_key_path, 'w') as key_file:
            key_file.write(response['KeyMaterial'])

        print(f"Key pair '{key_pair_name}' created successfully. Private key saved to '{private_key_path}'.")

    else:
        response = ec2.create_key_pair(KeyName=key_pair_name)
        os.makedirs(key_pair_path)
        private_key_path = f'{key_pair_path}/{key_pair_name}.pem'
        with open(private_key_path, 'w') as key_file:
            key_file.write(response['KeyMaterial'])

        print(f"Key pair '{key_pair_name}' created successfully. Private key saved to '{private_key_path}'.")


create_key_pem_ec2()