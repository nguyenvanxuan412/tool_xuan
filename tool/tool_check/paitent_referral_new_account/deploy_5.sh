#!/bin/bash

# Define a list of strings
string_list=(
"-target=aws_secretsmanager_secret.bitbucket_api_key"
"-target=aws_secretsmanager_secret.ci_slack_incoming_web_hook_url"
"-target=aws_secretsmanager_secret.security_email_app"
"-target=aws_ssm_parameter.config"
"-target=module.account.data.aws_elb_service_account.main"
"-target=module.account.data.http.aws_ip_ranges"
"-target=module.account.aws_cloudformation_stack.rackspace[0]"
"-target=module.account.aws_dynamodb_table.backend[0]"
"-target=module.account.aws_ecs_cluster.fargate[0]"
"-target=module.account.aws_ecs_cluster_capacity_providers.fargate[0]"
"-target=module.account.aws_security_group.AllowAll[0]"
"-target=module.account.aws_security_group.LBWeb[0]"
"-target=module.account.aws_security_group.RDS[0]"
"-target=module.account.aws_security_group.VPCInternal[0]"
"-target=module.account.aws_security_group.Web[0]"
"-target=module.account.aws_security_group.no_in_all_out[0]"
"-target=module.account.aws_security_group.s3_access[0]"
"-target=module.account.aws_security_group.secretsmanager[0]"
"-target=module.account.aws_ssm_parameter.core_s3_devops_build_bucket"
"-target=module.aws_alert.data.archive_file.aws_alert"
"-target=module.aws_alert.data.aws_secretsmanager_secret.aws_alert"
"-target=module.aws_alert.aws_cloudwatch_event_rule.aws_alert"
"-target=module.aws_alert.aws_cloudwatch_event_rule.aws_alert_ecs"
"-target=module.aws_alert.aws_cloudwatch_event_target.aws_alert_ecs"
"-target=module.aws_alert.aws_cloudwatch_event_target.s3_create_delete_alert"
"-target=module.aws_alert.aws_iam_role.aws_alert"
"-target=module.aws_alert.aws_iam_role_policy_attachment.aws_alert"
"-target=module.aws_alert.aws_lambda_function.aws_alert"
"-target=module.aws_alert.aws_lambda_permission.aws_alert"
"-target=module.aws_alert.aws_lambda_permission.aws_alert_ecs"
"-target=module.iam_role.aws_iam_policy.all"
"-target=module.iam_role.aws_iam_role.all"
"-target=module.iam_role.aws_iam_role_policy_attachment.all"
"-target=module.slack_post.data.archive_file.slack_post"
"-target=module.slack_post.aws_iam_role.slack_post"
"-target=module.slack_post.aws_iam_role_policy_attachment.slack_post"
"-target=module.slack_post.aws_lambda_function.slack_post"
"-target=module.account.module.ecs-event-bridge.aws_cloudwatch_event_rule.ecs_event_log[0]"
"-target=module.account.module.ecs-event-bridge.aws_cloudwatch_event_target.ecs_event_log[0]"
"-target=module.account.module.ecs-event-bridge.aws_cloudwatch_log_group.ecs_event_log[0]"


)

if [ "$#" -lt 1 ]; then
  echo "Error: At least one argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"
d tf acc:$arg1 -- init && d tf acc:$arg1 -- apply "${string_list[@]}"

tft_target="d tft /home/xuanit/projects/devops/infra/acc/tenpoint7/patient_referral_group.tf"
output_variable=$(bash -c "$tft_target")

# Print or use the captured output
echo "Output: $output_variable"

/home/xuanit/projects/tools/tools/bin/d tf acc:tenpoint7 -- apply $output_variable




