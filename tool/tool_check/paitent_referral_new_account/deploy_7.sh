#!/bin/bash

# Define a list of strings
string_list=(
"-target=module.account.module.s3.aws_s3_bucket.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket.all[\"devops\"]"
"-target=module.account.module.s3.aws_s3_bucket.all[\"log\"]"
"-target=module.account.module.s3.aws_s3_bucket_lifecycle_configuration.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket_lifecycle_configuration.all[\"devops\"]"
"-target=module.account.module.s3.aws_s3_bucket_lifecycle_configuration.all[\"log\"]"
"-target=module.account.module.s3.aws_s3_bucket_ownership_controls.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket_ownership_controls.all[\"devops\"]"
"-target=module.account.module.s3.aws_s3_bucket_ownership_controls.all[\"log\"]"
"-target=module.account.module.s3.aws_s3_bucket_policy.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket_policy.all[\"log\"]"
"-target=module.account.module.s3.aws_s3_bucket_public_access_block.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket_public_access_block.all[\"devops\"]"
"-target=module.account.module.s3.aws_s3_bucket_public_access_block.all[\"log\"]"
"-target=module.account.module.s3.aws_s3_bucket_server_side_encryption_configuration.all[\"build\"]"
"-target=module.account.module.s3.aws_s3_bucket_server_side_encryption_configuration.all[\"devops\"]"
"-target=module.account.module.s3.aws_s3_bucket_server_side_encryption_configuration.all[\"log\"]"
"-target=module.account.module.s3.aws_ssm_parameter.all[\"build\"]"
"-target=module.account.module.s3.aws_ssm_parameter.all[\"devops\"]"
"-target=module.account.module.s3.aws_ssm_parameter.all[\"log\"]"
)

if [ "$#" -lt 1 ]; then
  echo "Error: At least one argument is required."
  echo "Usage: $0 <aws_profile>"
  exit 1
fi

arg1="$1"
# Print the entire list
echo "${string_list[@]}"
echo "${arg1}"
d tf acc:$arg1 -- init && d tf acc:$arg1 -- apply "${string_list[@]}"




