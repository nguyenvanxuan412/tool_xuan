import click
from click import echo
from tool_check.utils.cmd_utils import run_cmd
from tool_check.utils.aws_profile_client import aws_profile_client
import rich

list_of_secret = {
    "pr-prod-PRONOCIS_USERNAME": "TesDummy",
    "pr-prod-PRONOCIS_PASSWORD": "6hEUYTGTx593gLX",
    # "pr-prod-ECW_CREDENTIALS": '"username":"david","password":"EXAMPLE-PASSWORD"',
    "patient-referral-pr-prod_SENTRY_DSN": "https://fb4dc80d33b2470c9340c1b356bacffb@o4504211752550400.ingest.sentry.io/4504971615272960",
    "patient-referral-pr-prod_AZURE_OPENAPI_KEY": "4968bf1967f2439d89b477a5601327ef"
}



# @click.command()
# @click.argument("username")
# @click.argument("password")
# @click.argument("ecw_credentials")
# @click.argument("sentry")
# @click.argument("azure_openapi_key")
@click.command()
@click.argument("profile")
def secret_client(profile):
    """
    tool_xuan pr-secret <aws_profile>
    """
    # profile, key = profile_key.split('/')
    echo(click.style(f'AWS Profile: {profile}', fg='green'))
    with rich.get_console().status("Connect AWS Secret Manager..."):
        for key, value in list_of_secret.items():
            aws_profile = aws_profile_client(profile)
            connect = aws_profile.client('secretsmanager')
            response = connect.put_secret_value(
                SecretId=key,
                SecretString=value,
            )
            print(response)
    with rich.get_console().status("Connect AWS Secret Manager..."):
        aws_profile = aws_profile_client(profile)
        connect = aws_profile.client('secretsmanager')
        response = connect.put_secret_value(
            SecretId="pr-prod-ECW_CREDENTIALS",
            SecretString='{"username":"tenpoint7","password":"Ane5!sP@in"}',
        )

    # return response
