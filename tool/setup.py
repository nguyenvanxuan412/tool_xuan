from setuptools import setup

# sudo apt-get install xclip
setup(
    name="CLI Check",
    version="0.0.1",
    description="Greet The World.",
    install_requires=[
        "mss",
        "pyperclip",
        "boto3",
        "awscost",
        "rich",
        "click",
        "googletrans~=4.0.0rc1",
        "pytube"
    ],
    packages=["tool_check",
              "tool_check/awscost",
              "tool_check/code_sum",
              "tool_check/utils",
              "tool_check/xuan_abc",
              "tool_check/ec2_start_slack_post",
              "tool_check/neospine_deploy",
              "tool_check/tp7_credentials",
              "tool_check/openvpn",
              "tool_check/aws_lab",
              "tool_check/aws_profile_secret_value_pr",

              ],
    py_modules=["main"],
    entry_points={
        "console_scripts": ["main=main:cli"],
    },
)
